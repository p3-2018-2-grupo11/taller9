#include<sys/types.h>
#include <string.h>
#include<sys/stat.h>
#include "csapp.h"

void echo(int connfd);
enum estados{EXIT=0,DESCONECTADO,ESCUCHANDO,CONECTADO,RESP_OK,ENVIA_ARCH} estado; 
int main(int argc, char **argv)
{
	int listenfd, connfd,fd1,n;
	char buf[MAXLINE];
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	char *port,nombre_arch[MAXLINE];
	rio_t rio;
	struct stat mi_stat;

	if (argc != 3) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[2];
	estado=DESCONECTADO;
	
	while (estado) {
		switch(estado){
		case(DESCONECTADO):
			listenfd = Open_listenfd(port);
			estado=ESCUCHANDO;
			break;
		case(ESCUCHANDO):
			clientlen = sizeof(clientaddr);
			connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
			/* Determine the domain name and IP address of the client */
			
			estado=CONECTADO;
			break;
		case(CONECTADO):
			Rio_readinitb(&rio, connfd);
			read(connfd,nombre_arch,MAXLINE);			
			estado=RESP_OK;
			break;
		case(RESP_OK):
			
			fd1=Open(nombre_arch,O_RDONLY,0777);
			if(fd1<0){
				Rio_writen(connfd, "\n",1);
				Close(connfd);
				Close(fd1);
				estado=ESCUCHANDO;			
			}
			else{
				stat(nombre_arch,&mi_stat);
				Rio_writen(connfd, "ok", 2*sizeof(char));
			
				estado=ENVIA_ARCH;}
			break;
		case(ENVIA_ARCH):
			while( (n=Rio_readn(fd1,buf,MAXLINE)) >0){
				Rio_writen(connfd,buf,n);}
			Close(connfd);
			Close(fd1);
			//estado=ESCUCHANDO;
			estado=EXIT;
			break;
		case(EXIT):
			break;
	
		}	
	}
	exit(0);
}