all: cliente servidor

cliente: src/cliente.c csapp.o
	gcc -O2 -Wall -I . -o bin/cliente src/cliente.c obj/csapp.o -lpthread

servidor: src/servidor.c obj/csapp.o
	gcc -O2 -Wall -I . -o bin/servidor src/servidor.c obj/csapp.o -lpthread

csapp.o: src/csapp.c
	gcc -O2 -Wall -I . -c src/csapp.c -o obj/csapp.o


